Test Data Generator
=============

This application component is a tool for generating fake data for testing and development purposes.

There are multiple modes that are supported, either for creating a specific type of fake data, or for generating a table with specific columns and content within them.

Generation of specific types is donw by utilizing the Faker python library.

**Table of contents:**

[TOC]

Configuration
=============

- Output table name (name) - [REQ]
- Is custom dataset (is_custom_dataset) - [REQ] 0/1 0- if is a template dataset, 1- if is a custom specified dataset
- Number of rows (num_rows) - [REQ] number of rows in output table
- Dataset type (predefined_dataset_type) - [REQ] if custom dataset is not selected, a predetermined data template must be used from the following list :
    - "Simple contact data"
    - "Location data"
- Custom data specification (custom_data) - [REQ] if custom data is selected, you must specify the following:
    - column_name
    - data_type :
        - Custom value 
        - Random integer 
        - Random float
        - Random string
        - Date
        - Datetime
        - First name
        - Last name
        - Name
        - Email
        - Address
        - City
        - Country
        - Latitude
        - Longitude
        - Currency
        - IBAN
        - Company name
    - default_value - Required for Custom value data type. The dafault value will be the same for every row in the column of the dataset
    - type_length - Required if datatype is random int, float, or string

Sample Configuration
--------------------
```json
{
  "parameters": {
    "is_custom_dataset": 0,
    "name": "exasol_contact_info_test",
    "num_rows": 1000,
    "predefined_dataset_type": "Simple contact data"
  }
}
```

```json
{
  "parameters": {
  "name": "testing_data",
  "is_custom_dataset": 1,
  "num_rows": 50,
  "custom_data": [
    {
      "column_name": "name",
      "data_type": "Name"
    },
    {
      "column_name": "email",
      "data_type": "Email"
    },
    {
      "column_name": "address",
      "data_type": "Address"
    }
  ]
}
}
```


Development
-----------

If required, change local data folder (the `CUSTOM_FOLDER` placeholder) path to
your custom path in the `docker-compose.yml` file:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    volumes:
      - ./:/code
      - ./CUSTOM_FOLDER:/data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Clone this repository, init the workspace and run the component with following
command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
git clone https://bitbucket.org/kds_consulting_team/kds-team.app-test-data-generator/src/master/ kds-team.app-test-data-generator
cd kds-team.app-test-data-generator
docker-compose build
docker-compose run --rm dev
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run the test suite and lint check using this command:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
docker-compose run --rm test
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Integration
===========

For information about deployment and integration with KBC, please refer to the
[deployment section of developers
documentation](https://developers.keboola.com/extend/component/deployment/)
