CONTACT_DATA_COLUMN_SETTINGS = [
    {
        "column_name": "name",
        "data_type": "Name"
    },
    {
        "column_name": "email",
        "data_type": "Email"
    },
    {
        "column_name": "address",
        "data_type": "Address"
    },
    {
        "column_name": "company",
        "data_type": "Company name"
    }
]
LOCATION_DATA_COLUMN_SETTINGS = [
    {
        "column_name": "company",
        "data_type": "Company name"
    },
    {
        "column_name": "latitude",
        "data_type": "Latitude"
    },
    {
        "column_name": "longitude",
        "data_type": "Longitude"
    },
    {
        "column_name": "city",
        "data_type": "City"
    },
    {
        "column_name": "country",
        "data_type": "Country"
    },
    {
        "column_name": "date_added",
        "data_type": "Date"
    }
]
