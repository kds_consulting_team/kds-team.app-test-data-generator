from .column_validator import ColumnValidator
from faker import Faker
from random import randint, uniform, choice
import string


class ColumnSettingsException(Exception):
    pass


class DataGenerator:
    def __init__(self, column_settings):
        self.validate_column_settings(column_settings)
        self.column_settings = column_settings

    @staticmethod
    def validate_column_settings(column_settings):
        col_validator = ColumnValidator()
        col_validator.validate_columns(column_settings)
        if col_validator.errors:
            raise ColumnSettingsException(f"Column settings are invalid, "
                                          f"the following errors occurred {col_validator.errors}")

    def create_custom_dataset(self, num_rows):
        test_data = []
        fake = Faker(['en_US'])
        for _ in range(num_rows):
            test_data.append(self._populate_custom_data_row(fake))
        return test_data

    def _populate_custom_data_row(self, fake):
        profile = fake.profile()
        data_row = {}
        for custom_column in self.column_settings:
            if custom_column["data_type"] == "Custom value":
                data_row[custom_column["column_name"]] = custom_column["default_value"]
            elif custom_column["data_type"] == "Name":
                data_row[custom_column["column_name"]] = profile["name"]
            elif custom_column["data_type"] == "First name":
                data_row[custom_column["column_name"]] = profile["name"].split(" ")[0]
            elif custom_column["data_type"] == "Last name":
                data_row[custom_column["column_name"]] = profile["name"].split(" ")[1]
            elif custom_column["data_type"] == "Address":
                data_row[custom_column["column_name"]] = profile["address"].replace("\n", " ")
            elif custom_column["data_type"] == "Email":
                data_row[custom_column["column_name"]] = profile["mail"]
            elif custom_column["data_type"] == "Random integer":
                data_row[custom_column["column_name"]] = randint(0, 10 ** (int(custom_column["type_length"]) - 1))
            elif custom_column["data_type"] == "Random float":
                float_num = round(uniform(0, 9), int(custom_column["type_length"]))
                data_row[custom_column["column_name"]] = float_num
            elif custom_column["data_type"] == "Random string":
                data_row[custom_column["column_name"]] = self.generate_rand_string(int(custom_column["type_length"]))
            elif custom_column["data_type"] == "City":
                data_row[custom_column["column_name"]] = fake.city()
            elif custom_column["data_type"] == "Country":
                data_row[custom_column["column_name"]] = fake.country()
            elif custom_column["data_type"] == "Date":
                data_row[custom_column["column_name"]] = fake.date()
            elif custom_column["data_type"] == "Datetime":
                data_row[custom_column["column_name"]] = fake.date_time()
            elif custom_column["data_type"] == "Latitude":
                data_row[custom_column["column_name"]] = fake.latitude()
            elif custom_column["data_type"] == "Longitude":
                data_row[custom_column["column_name"]] = fake.longitude()
            elif custom_column["data_type"] == "Currency":
                data_row[custom_column["column_name"]] = fake.currency()
            elif custom_column["data_type"] == "IBAN":
                data_row[custom_column["column_name"]] = fake.iban()
            elif custom_column["data_type"] == "Company name":
                data_row[custom_column["column_name"]] = fake.company()
        return data_row

    @staticmethod
    def generate_rand_string(string_length):
        return ''.join(choice(string.ascii_letters) for _ in range(string_length))
