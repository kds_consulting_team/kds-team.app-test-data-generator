class ColumnValidator:
    def __init__(self):
        self.errors = []

    def validate_columns(self, column_specs):
        for column_spec in column_specs:
            self.validate_column(column_spec)

    def validate_column(self, column_spec):
        self.validate_column_name(column_spec['column_name'])
        if "type_length" in column_spec:
            self.validate_integer(column_spec["type_length"], column_spec['column_name'])

    def validate_column_name(self, column_name):
        if not column_name.replace("_", "").isalnum():
            self.errors.append(
                f"The column name : {column_name} is invalid, "
                f"make sure it is only alphanumeric characters and underscores")

    def validate_integer(self, value, value_name):
        try:
            int(value)
        except ValueError:
            self.errors.append(
                f"The column name : {value_name} has invalid type length, "
                f"{value} is not an integer")

    def get_errors(self):
        return self.errors
