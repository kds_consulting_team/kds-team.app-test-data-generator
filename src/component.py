import logging
import csv
from data_generator import DataGenerator, ColumnSettingsException
from data_generator import CONTACT_DATA_COLUMN_SETTINGS
from data_generator import LOCATION_DATA_COLUMN_SETTINGS
from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException

KEY_TABLE_NAME = "name"
KEY_IS_CUSTOM_DATASET = "is_custom_dataset"
KEY_PREDEFINED_DATASET_TYPE = "predefined_dataset_type"
KEY_NUM_ROWS = "num_rows"
KEY_CUSTOM_DATA = "custom_data"

KEY_COLUMN_NAME = "column_name"
KEY_DATA_TYPE = "data_type"
KEY_DEFAULT_VALUE = "default_value"
KEY_LENGTH = "type_length"

REQUIRED_PARAMETERS = [KEY_TABLE_NAME, KEY_NUM_ROWS]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):

    def __init__(self):
        super().__init__(required_parameters=REQUIRED_PARAMETERS,
                         required_image_parameters=REQUIRED_IMAGE_PARS)

    def run(self):
        params = self.configuration.parameters
        output_bucket = self.get_bucket_name()
        output_file_name = params.get(KEY_TABLE_NAME, 'testing_data')

        if not output_file_name.replace("_", "").isalnum():
            raise UserException("Output file name must be composed of only alphanumeric characters and underscores")

        logging.info(f"Saving to bucket : {output_bucket}  with filename : {output_file_name}")
        table = self.create_out_table_definition(output_file_name,
                                                 destination=f'{output_bucket}.{output_file_name}')

        num_rows = self.convert_to_int(params.get(KEY_NUM_ROWS, "10"))
        if num_rows < 1:
            raise UserException("Number of rows must be atleast 1")

        dataset = self.create_test_data(num_rows)

        fieldnames = list(dataset[0].keys())
        table.columns = fieldnames
        self.write_data(table, dataset, fieldnames)
        self.write_manifest(table)

    def get_bucket_name(self) -> str:
        config_id = self.environment_variables.config_id
        if not config_id:
            config_id = "000000000"
        bucket_name = f"in.c-testing-data-{config_id}"
        return bucket_name

    def create_test_data(self, num_rows):
        is_custom_dataset = self.configuration.parameters.get(KEY_IS_CUSTOM_DATASET, [])
        predefined_dataset_type = self.configuration.parameters.get(KEY_PREDEFINED_DATASET_TYPE, [])
        if is_custom_dataset:
            logging.info("Generating data with custom headers and values")
            column_settings = self.configuration.parameters.get(KEY_CUSTOM_DATA, [])
        elif predefined_dataset_type == "Simple contact data":
            logging.info("Generating Simple Contact Data")
            column_settings = CONTACT_DATA_COLUMN_SETTINGS
        elif predefined_dataset_type == "Location data":
            logging.info("Generating Simple Contact Data")
            column_settings = LOCATION_DATA_COLUMN_SETTINGS
        else:
            raise UserException(f"Predefined dataset type ({predefined_dataset_type}) is not implemented")

        try:
            data_gen = DataGenerator(column_settings)
        except ColumnSettingsException as col_exc:
            raise UserException(col_exc) from col_exc
        dataset = data_gen.create_custom_dataset(num_rows)
        return dataset

    @staticmethod
    def write_data(table, test_data, fieldnames):
        logging.info("Saving generated data")
        with open(table.full_path, 'w', newline='\n') as output:
            writer = csv.DictWriter(output, fieldnames, quoting=csv.QUOTE_ALL)
            writer.writerows(test_data)

    @staticmethod
    def convert_to_int(number_str):
        if type(number_str) is int:
            return number_str
        try:
            return int(number_str)
        except ValueError:
            raise UserException("Number of rows parameter must be an integer")


if __name__ == "__main__":
    try:
        comp = Component()
        comp.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
